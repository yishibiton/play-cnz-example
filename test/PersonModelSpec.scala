/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import java.util.UUID

import models.Person
import play.api.libs.json.Json

/** Person model tests
 */
class PersonModelSpec extends ModelSpec {
    val idUnknown = UUID.randomUUID()
    val idGerber  = UUID.fromString("dd27f951-8070-45f5-979a-335655f8a515")

    "Person model" should {
        "throw IllegalArgumentException for stateCode too short" in {
            assertThrows[IllegalArgumentException] {
                Person(stateCode = Some("K"))
            }
        }
        "throw IllegalArgumentException for stateCode too long" in {
            assertThrows[IllegalArgumentException] {
                Person(stateCode = Some("KYYY"))
            }
        }
        "throw IllegalArgumentException for countryCode too short" in {
            assertThrows[IllegalArgumentException] {
                Person(countryCode = Some("U"))
            }
        }
        "throw IllegalArgumentException for countryCode too long" in {
            assertThrows[IllegalArgumentException] {
                Person(countryCode = Some("USA"))
            }
        }
        "find person by ID" in {
            val maybeRes = Person.find(idGerber)
            maybeRes.isSuccess      mustBe true
            maybeRes.success.value  mustBe defined
            val foundPerson:Person = maybeRes.success.value.value
            foundPerson.id          mustBe defined
            foundPerson.id          mustBe (Some(idGerber))
            foundPerson.whenCreated mustBe defined
            foundPerson.nameLast    mustBe (Some("Gerber"))
            foundPerson.nameFirst   mustBe (Some("Steve"))
        }
        "not find by an unknown ID" in {
            val maybeRes = Person.find(idUnknown)
            maybeRes.isSuccess     mustBe      true
            maybeRes.success.value must not be defined
        }
        "list persons" in {
            val maybeRes = Person.list()
            maybeRes.isSuccess     mustBe true
        }
        "convert to simple JSON" in {
            implicit val serializer = models.Person.SimpleJsonWrites
            val person = Person(
                nameLast = Some("Last")
            )
            val json = Json.toJson(person)
            (json \ "name-last").as[String] mustBe ("Last")
        }
        "convert to JSON API" in {
            implicit val serializer = models.Person.JsonApiWrites
            val person = Person(
                nameLast = Some("Last")
            )
            val json = Json.toJson(person)
            (json \ "attributes" \ "name-last").as[String] mustBe ("Last")
        }
    }
}
