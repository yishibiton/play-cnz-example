/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import scala.concurrent.Future

import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.Result
import play.api.test.{FakeRequest, Injecting}
import play.api.test.Helpers.{charset, contentAsString, contentType, defaultAwaitTimeout, route, status, writeableOf_AnyContentAsEmpty, GET, OK}

/** Application controller tests
 */
class ApplicationControllerSpec extends PlaySpec with GuiceOneAppPerSuite with Injecting {
    /** URL encode a string with charset UTF-8.
     *
     *  @param s The string to encode
     *  @return  s encoded for URLs
     */
    def enc(s:String) : String = java.net.URLEncoder.encode(s, "UTF-8")

    "index route" should {
        "not display path if path is empty" in {
            val path = ""
            val controller = inject[controllers.ApplicationController]
            val result:Future[Result] = controller.index(path).apply(FakeRequest())
            val bodyText:String = contentAsString(result)
            bodyText must not include ("The path is ")
        }
        "display path if path is not empty" in {
            val path = "this/is/a/path"
            val controller = inject[controllers.ApplicationController]
            val result:Future[Result] = controller.index(path).apply(FakeRequest())
            val bodyText:String = contentAsString(result)
            bodyText must include (s"The path is $path.")
        }
    }
    "list route" should {
        "not display filter if filter is empty" in {
            val Some(result) = route(app, FakeRequest(GET, "/list"))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must not include("<h2>Filter</h2>")
        }
        "display filter if filter is not empty" in {
            val Some(result) = route(app, FakeRequest(GET, s"/list?${enc("filter[Person.nameLast]")}=Howard&${enc("filter[Person.nameFirst]")}=Robert&${enc("filter[Person.nameMiddle]")}=E."))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must include("<h2>Filter</h2>")
            contentAsString(result) must include("<li>Person.nameLast Howard</li>")
            contentAsString(result) must include("<li>Person.nameFirst Robert</li>")
            contentAsString(result) must include("<li>Person.nameMiddle E.</li>")
        }
        "not display page if page is empty" in {
            val Some(result) = route(app, FakeRequest(GET, "/list"))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must not include("<p>Page ")
        }
        "display page if page is not empty" in {
            val Some(result) = route(app, FakeRequest(GET, s"/list?${enc("page[number]")}=1&${enc("page[size]")}=10"))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must include("<p>Page 1 of size 10.</p>")
        }
        "not display sort if sort is empty" in {
            val Some(result) = route(app, FakeRequest(GET, "/list"))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must not include("<h2>Sort Order</h2>")
        }
        "display sort if sort is not empty" in {
            val Some(result) = route(app, FakeRequest(GET, s"/list?sort=${enc("Person.nameLast,-Person.nameFirst,Person.nameMiddle")}"))
            status(result)          mustEqual OK
            contentType(result)     mustEqual Some("text/html")
            charset(result)         mustEqual Some("utf-8")
            contentAsString(result) must include("<h2>Sort Order</h2>")
            contentAsString(result) must include("<li>Person.nameLast</li>")
            contentAsString(result) must include("<li>Person.nameFirst desc</li>")
            contentAsString(result) must include("<li>Person.nameMiddle</li>")
        }
    }
}
