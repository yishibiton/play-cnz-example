/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import java.util.UUID

import com.cnz.play.http.MimeType
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.{Json, JsValue}
import play.api.test.{FakeRequest, Injecting}
import play.api.test.Helpers.{charset, contentAsString, contentType, defaultAwaitTimeout, route, status, writeableOf_AnyContentAsEmpty, GET, NOT_FOUND, OK, UNSUPPORTED_MEDIA_TYPE}

/** Person controller tests
 */
class PersonControllerSpec extends PlaySpec with GuiceOneAppPerSuite with Injecting {
    val idGerber  = UUID.fromString("dd27f951-8070-45f5-979a-335655f8a515")
    val idUnknown = UUID.randomUUID()

    "list route" should {
        "contain two persons using JSON API" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, "/api/Person").
                    withHeaders("Accept" -> MimeType.JsonApi.value)
            )
            status(result)      mustEqual OK
            contentType(result) mustEqual Some(MimeType.JsonApi.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            (json \ "data").as[Seq[JsValue]].length mustBe (2)
        }
        "contain two persons using simple JSON" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, "/api/Person").
                    withHeaders("Accept" -> MimeType.Json.value)
            )
            status(result)      mustEqual OK
            contentType(result) mustEqual Some(MimeType.Json.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            json.as[Seq[JsValue]].length mustBe (2)
        }
        "result in UNSUPPORTED_MEDIA_TYPE using HTML" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, "/api/Person").
                    withHeaders("Accept" -> MimeType.Html.value)
            )
            status(result) mustEqual UNSUPPORTED_MEDIA_TYPE
        }
    }
    "find route" should {
        "find existing Person using JSON API" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, s"/api/Person/${idGerber.toString}").
                    withHeaders("Accept" -> MimeType.JsonApi.value)
            )
            status(result)      mustEqual OK
            contentType(result) mustEqual Some(MimeType.JsonApi.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            (json \ "data" \ "attributes" \ "name-last").as[String] mustBe ("Gerber")
        }
        "find existing Person using simple JSON" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, s"/api/Person/${idGerber.toString}").
                    withHeaders("Accept" -> MimeType.Json.value)
            )
            status(result)      mustEqual OK
            contentType(result) mustEqual Some(MimeType.Json.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            (json \ "name-last").as[String] mustBe ("Gerber")
        }
        "not find person from unknown ID using JSON API" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, s"/api/Person/${idUnknown.toString}").
                    withHeaders("Accept" -> MimeType.JsonApi.value)
            )
            status(result)      mustEqual NOT_FOUND
            contentType(result) mustEqual Some(MimeType.JsonApi.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            (json \ "errors" \ 0 \ "status").as[String] mustBe ("404")
        }
        "not find person from unknown ID using simple JSON" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, s"/api/Person/${idUnknown.toString}").
                    withHeaders("Accept" -> MimeType.Json.value)
            )
            status(result)      mustEqual NOT_FOUND
            contentType(result) mustEqual Some(MimeType.Json.value)
            charset(result)     mustEqual Some("utf-8")
            val json = Json.parse(contentAsString(result))
            (json \ "errors" \ 0 \ "status").as[String] mustBe ("404")
        }
        "result in UNSUPPORTED_MEDIA_TYPE using HTML" in {
            val Some(result) = route(
                app,
                FakeRequest(GET, s"/api/Person/${idUnknown.toString}").
                    withHeaders("Accept" -> MimeType.Html.value)
            )
            status(result) mustEqual UNSUPPORTED_MEDIA_TYPE
        }
    }
}
