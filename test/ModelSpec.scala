/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import org.scalatest.TryValues
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.test.Injecting

/** Trait for model tests
 */
trait ModelSpec extends PlaySpec with GuiceOneAppPerSuite with ScalaFutures with TryValues with Injecting {
    final val calendarType   = "iso8601"

    def simpleDate(year:Int, month:Int, day:Int) : java.util.Date = {
        new java.util.Calendar.Builder().setCalendarType(calendarType).setDate(year, month, day).build().getTime()
    }
}
