/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

pipeline {
    agent any
    stages {
        /*
         * This is a workaround for https://issues.jenkins-ci.org/browse/JENKINS-42475.
         * You can use sbt-plugin with this workaround if you set the
         * value of "sbt launch jar" to the SBT installation directory.
         * The plugin will complain about "sbt-launch.jar not found",
         * but the build will work.  Alternatively, use the Custom Tools
         * plugin (https://plugins.jenkins.io/custom-tools-plugin).
         */
        stage('Initialize') {
            steps {
                echo 'Initializing ...'
                script {
                    def sbtHome = tool 'sbt'
                    env.sbt= "${sbtHome}/bin/sbt -Dsbt.log.noformat=true"
                }
            }
        }
        stage('Build') {
            steps {
                echo 'Building ...'
                sh "${sbt} clean dist"
                archiveArtifacts artifacts: 'target/universal/*.zip', fingerprint: true
            }
        }
        stage('Test') {
            steps {
                echo 'Testing ...'
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying ...'
            }
        }
    }
}
