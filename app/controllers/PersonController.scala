/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

package controllers

import java.util.UUID
import javax.inject.Inject

import scala.util.{Failure, Success}

import com.cnz.play.http.MimeType
import com.cnz.play.mvc.{ErrorDocument, JsonApiDocument, QueryFilter, QueryPage, QuerySort, SimpleJsonDocument}
import com.cnz.play.mvc.JsonApiDocument.AcceptsJsonApi
import com.cnz.play.mvc.SimpleJsonDocument.AcceptsSimpleJson
import models.Person
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents, Result}

/** The Person controller
 *
 *  Provides route handlers for API endpoints related to the Person model.
 *
 *  Each API endpoint serves either simple JSON or a JSON API document
 *  based on the Accept header.  Since an API client will normally
 *  specify a single content type, this intentionally does not try to be
 *  observant of the "q" parameters used for specifying relative
 *  preference levels for different content types.
 *
 *  For an alternate method that selects from amount multiple accepted
 *  content types, which is appropriate when serving clients not
 *  specifically written for the serving application , see
 *  play.api.mvc.Rendering.render.
 *
 *  @param components The base controller components
 */
class PersonController @Inject()(
    val components : ControllerComponents
) extends AbstractController(components) {
    implicit private val defaultMarkerContext = play.api.MarkerContext.NoMarker
    private          val logger               = play.api.Logger(this.getClass)

    private def doFind(id:UUID, documentFormatter:(Person) => (MimeType, String), errorFormatter:(ErrorDocument) => (MimeType, String)) : Result = {
        Person.find(id) match {
            case Failure(e)            => {
                val errorId = UUID.randomUUID()
                logger.error(s"$errorId:  Error finding person ID $id: $e")
                val httpError = ErrorDocument(id = Some(errorId), statusCode = INTERNAL_SERVER_ERROR, exception = e)
                val response = errorFormatter(httpError)
                InternalServerError(response._2).as(withCharset(response._1.value))
            }
            case Success(None)         => {
                logger.debug(s"Person ID $id not found.")
                val httpError = ErrorDocument(id = Some(UUID.randomUUID()), statusCode = NOT_FOUND, exception = new Throwable(s"Person ID $id not found."))
                val response = errorFormatter(httpError)
                NotFound(response._2).as(withCharset(response._1.value))
            }
            case Success(Some(person)) => {
                logger.debug(s"Person ID $id found = $person.")
                val response = documentFormatter(person)
                Ok(response._2).as(withCharset(response._1.value))
            }
        }
    }

    /** Find a Person by ID.
     *
     *  @param id ID
     *  @return   action
     */
    def find(id:UUID) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} find(id = $id)")

        def formatJsonApiDocument(person:Person) : (MimeType, String) = {
            implicit val serializer = Person.JsonApiWrites
            val jsonApiDocument = JsonApiDocument(
                self = Some(controllers.routes.PersonController.find(id = id).absoluteURL()),
                data = Json.toJson(person)
            )
            (MimeType.JsonApi, Json.toJson(jsonApiDocument).toString)
        }

        def formatSimpleJsonDocument(person:Person) : (MimeType, String) = {
            implicit val serializer = Person.SimpleJsonWrites
            (MimeType.Json, Json.toJson(person).toString)
        }

        request match {
            case AcceptsJsonApi()    => {
                doFind(id, formatJsonApiDocument, JsonApiDocument.formatJsonApiErrorDocument)
            }
            case AcceptsSimpleJson() => {
                doFind(id, formatSimpleJsonDocument, SimpleJsonDocument.formatSimpleJsonErrorDocument)
            }
            case _                   => {
                UnsupportedMediaType(s"Expecting Accept ${MimeType.Json} or ${MimeType.JsonApi}.")
            }
        }
    }

    private def doList(filter:Option[QueryFilter], page:Option[QueryPage], sort:Option[QuerySort], documentFormatter:(Seq[Person]) => (MimeType, String), errorFormatter:(ErrorDocument) => (MimeType, String)) : Result = {
        Person.list(filter, page, sort) match {
            case Failure(e)       => {
                val errorId = UUID.randomUUID()
                logger.error(s"$errorId:  Error finding person list: $e")
                val httpError = ErrorDocument(id = Some(errorId), statusCode = INTERNAL_SERVER_ERROR, exception = e)
                val response = errorFormatter(httpError)
                InternalServerError(response._2).as(withCharset(response._1.value))
            }
            case Success(persons) => {
                logger.debug("Person list found.")
                val response = documentFormatter(persons)
                Ok(response._2).as(withCharset(response._1.value))
            }
        }
    }

    /** Retrieve a list of Persons.
     *
     *  Generate an HTTP response containing a list of a single record of dummy
     *  data.  The list will be in either plain JSON or JSON API format, as
     *  determined by the Accept header in the request.
     *
     *  Other than the links in the JSON API response, the response here will be
     *  the same regardless of the query parameters.  In a real application, the
     *  filtering, pagination, and sorting would be performed by the relevant
     *  model.
     *
     *  @param filter The filter from the request query string
     *  @param page   The page from the request query string
     *  @param sort   The sort order from the request query string
     *  @return       HTTP response
     */
    def list(filter:Option[QueryFilter], page:Option[QueryPage], sort:Option[QuerySort]) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} list(filter = $filter, page = $page, sort = $sort)")

        def formatJsonApiDocument(persons:Seq[Person]) : (MimeType, String) = {
            implicit val serializer = Person.JsonApiWrites
            val jsonApiDocumentBase = JsonApiDocument(
                self  = Some(controllers.routes.PersonController.list(filter = filter, page = page, sort = sort).absoluteURL()),
                data  = Json.toJson(persons)
            )
            val jsonApiDocument = page match {
                case Some(p) => jsonApiDocumentBase.copy(
                    first = Some(controllers.routes.PersonController.list(filter = filter, page = Some(p.first), sort = sort).absoluteURL()),
                    prev  = if (p.isFirst) None else Some(controllers.routes.PersonController.list(filter = filter, page = p.prev, sort = sort).absoluteURL()),
                    next  = Some(controllers.routes.PersonController.list(filter = filter, page = Some(p.next), sort = sort).absoluteURL())
                )
                case None    => jsonApiDocumentBase
            }
            (MimeType.JsonApi, Json.toJson(jsonApiDocument).toString)
        }

        def formatSimpleJsonDocument(persons:Seq[Person]) : (MimeType, String) = {
            implicit val serializer = Person.SimpleJsonWrites
            (MimeType.Json, Json.toJson(persons).toString)
        }

        request match {
            case AcceptsJsonApi()    => {
                doList(filter, page, sort, formatJsonApiDocument, JsonApiDocument.formatJsonApiErrorDocument)
            }
            case AcceptsSimpleJson() => {
                doList(filter, page, sort, formatSimpleJsonDocument, SimpleJsonDocument.formatSimpleJsonErrorDocument)
            }
            case _                   => {
                UnsupportedMediaType(s"Expecting Accept ${MimeType.Json} or ${MimeType.JsonApi}.")
            }
        }
    }
}
