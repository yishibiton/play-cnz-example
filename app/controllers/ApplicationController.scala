/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

package controllers

import javax.inject.Inject

import com.cnz.play.mvc.{QueryFilter, QueryPage, QuerySort}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

/** The main controller.
 *
 *  @param assetsFinder Asset finder
 *  @param components   The base controller components
 */
class ApplicationController @Inject()(
    val components : ControllerComponents
)(
    implicit val assetsFinder : AssetsFinder
) extends AbstractController(components) {
    implicit private val defaultMarkerContext = play.api.MarkerContext.NoMarker
    private          val logger               = play.api.Logger(this.getClass)

    /** The main route.
     *
     *  @param path The path portion of the request URI
     *  @return     HTTP response
     */
    def index(path:String) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} index(path = ${path})")
        Ok(views.html.index(path = path))
    }

    /** The list route.
     *
     *  @param filter The filter from the request query string
     *  @param page   The page from the request query string
     *  @param sort   The sort order from the request query string
     *  @return       HTTP response
     */
    def list(filter:Option[QueryFilter], page:Option[QueryPage], sort:Option[QuerySort]) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} list(filter = ${filter}, page = ${page}, sort = ${sort})")
        Ok(views.html.index(path = "", filter = filter, page = page, sort = sort))
    }
}
