/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

package models

import java.time.ZonedDateTime
import java.util.UUID

import scala.util.{Success, Try}

import com.cnz.play.mvc.{QueryFilter, QueryPage, QuerySort}
import play.api.libs.json.{Json, Writes}

/** Person model
 *
 *  id and whenCreated are Options to allow them to be assigned by the database.
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 *
 *  @param id          ID
 *  @param whenCreated creation timestamp
 *
 *  @param nameLast    last name
 *  @param nameFirst   first name
 *  @param nameMiddle  middle name
 *  @param nameSuffix  name suffix
 *  @param salutation  salutation
 *
 *  @param email       email address
 *  @param phoneHome   home phone number
 *  @param phoneMobile mobile phone number
 *  @param phoneWork   work phone number
 *  @param webSite     web site
 *
 *  @param address     street address
 *  @param city        city
 *  @param countyCode  county code
 *  @param stateCode   ISO 3166-2 (upper case)
 *  @param countryCode ISO 3166-1 alpha-2 (upper case)
 *  @param postalCode  postal code
 */
case class Person(
    id          : Option[UUID]          = None,
    whenCreated : Option[ZonedDateTime] = None,

    nameLast    : Option[String]        = None,
    nameFirst   : Option[String]        = None,
    nameMiddle  : Option[String]        = None,
    nameSuffix  : Option[String]        = None,
    salutation  : Option[String]        = None,

    email       : Option[String]        = None,
    phoneHome   : Option[String]        = None,
    phoneMobile : Option[String]        = None,
    phoneWork   : Option[String]        = None,
    webSite     : Option[String]        = None,

    address     : Option[String]        = None,
    city        : Option[String]        = None,
    countyCode  : Option[String]        = None,
    stateCode   : Option[String]        = None,
    countryCode : Option[String]        = None,
    postalCode  : Option[String]        = None
) {
    // Validate on construction.
    require(stateCode == None || stateCode.exists(2 to 3 contains _.length), s"Invalid stateCode specified: $stateCode")
    require(countryCode == None || countryCode.exists(_.length == 2), s"Invalid countryCode specified: $countryCode")
}

/** Person companion object
 */
object Person {
    implicit private val DefaultMarkerContext = play.api.MarkerContext.NoMarker
    private          val Logger               = play.api.Logger(this.getClass)
    private          val Now                  = java.time.ZonedDateTime.now(java.time.ZoneOffset.UTC)

    private val Data = Seq(
        Person(
            id          = Some(UUID.fromString("dd27f951-8070-45f5-979a-335655f8a515")),
            whenCreated = Some(Now),

            nameLast    = Some("Gerber"),
            nameFirst   = Some("Steve"),

            city        = Some("St. Louis"),
            stateCode   = Some("MO"),
            countryCode = Some("US")
        ),
        Person(
            id          = Some(UUID.fromString("2f0b8952-f967-4a2e-ac2c-cbac29e10a69")),
            whenCreated = Some(Now),

            nameLast    = Some("Kirby"),
            nameFirst   = Some("Jack"),

            city        = Some("New York"),
            stateCode   = Some("NY"),
            countryCode = Some("US")
        )
    )

    /** JSON API serializer for models.Person
     */
    val JsonApiWrites = new Writes[Person] {
        override def writes(o:Person) = Json.obj(
            "type" -> "person",
            "id"   -> o.id,
            "attributes" -> Json.obj(
                "when-created" -> o.whenCreated,

                "name-last"    -> o.nameLast,
                "name-first"   -> o.nameFirst,
                "name-middle"  -> o.nameMiddle,
                "name-suffix"  -> o.nameSuffix,
                "salutation"   -> o.salutation,

                "email"        -> o.email,
                "phone-home"   -> o.phoneHome,
                "phone-mobile" -> o.phoneMobile,
                "phone-work"   -> o.phoneWork,
                "web-site"     -> o.webSite,

                "address"      -> o.address,
                "city"         -> o.city,
                "county-code"  -> o.countyCode,
                "state-code"   -> o.stateCode,
                "country-code" -> o.countryCode,
                "postal-code"  -> o.postalCode
            )
        )
    }

    /** Simple JSON serializer for models.Person
     */
    val SimpleJsonWrites = new Writes[Person] {
        override def writes(o:Person) = Json.obj(
            "id"           -> o.id,
            "when-created" -> o.whenCreated,

            "name-last"    -> o.nameLast,
            "name-first"   -> o.nameFirst,
            "name-middle"  -> o.nameMiddle,
            "name-suffix"  -> o.nameSuffix,
            "salutation"   -> o.salutation,

            "email"        -> o.email,
            "phone-home"   -> o.phoneHome,
            "phone-mobile" -> o.phoneMobile,
            "phone-work"   -> o.phoneWork,
            "web-site"     -> o.webSite,

            "address"      -> o.address,
            "city"         -> o.city,
            "county-code"  -> o.countyCode,
            "state-code"   -> o.stateCode,
            "country-code" -> o.countryCode,
            "postal-code"  -> o.postalCode
        )
    }

    /** Find a Person by ID
     *
     *  The Try does nothing here, but would be needed for a real database.
     *
     *  @param id ID
     *  @return   optional Person with the specified ID, or an exception
     */
    def find(id:UUID) : Try[Option[Person]] = {
        Logger.debug(s"find(id = $id)")
        Success(Data.find(_.id.exists(_.equals(id))))
    }

    /** Find a list of Persons
     *
     *  The Try does nothing here, but would be needed for a real database.
     *
     *  The filtering, paging, and sorting are not currently implemented.
     *  These would normally be passed on to the database.
     *
     *  @param filter query filter
     *  @param page   pagination
     *  @param sort   sort order
     *  @return       list of Persons, or an exception
     */
    def list(filter:Option[QueryFilter] = None, page:Option[QueryPage] = None, sort:Option[QuerySort] = None) : Try[Seq[Person]] = {
        Logger.debug(s"list(filter = $filter, page = $page, sort = $sort)")
        Success(Data)
    }
}
