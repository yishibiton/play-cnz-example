addSbtPlugin("com.typesafe.play" % "sbt-plugin"            % "2.8.2")
addSbtPlugin("com.typesafe.sbt"  % "sbt-git"               % "1.0.0")
addSbtPlugin("org.scalastyle"    % "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scoverage"     % "sbt-scoverage"         % "1.6.1")

// sbt-web plugins https://github.com/sbt/sbt-web
addSbtPlugin("org.irundaia.sbt"  % "sbt-sassify"           % "1.5.1")
