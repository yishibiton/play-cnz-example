/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

name := "play-cnz-example"

version := "1.2.0"

maintainer := "lfrost@cnz.com"

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

lazy val root = (project in file(".")).
    enablePlugins(PlayScala, SbtWeb).
    settings(
        play.sbt.routes.RoutesKeys.routesImport ++= Seq(
            "java.util.UUID",
            "com.cnz.play.mvc._"
        )
    )

scalaVersion := "2.13.3"

// https://docs.scala-lang.org/overviews/compiler-options/
scalacOptions ++= Seq(
    // Standard Settings
    "-deprecation",             // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",       // Specify character encoding used by source files.
    "-explaintypes",            // Explain type errors in more detail.
    "-feature",                 // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked",               // Enable additional warnings where generated code depends on assumptions.

    // Advanced Settings

    // Warning Settings
    "-Ywarn-dead-code",         // Warn when dead code is identified.
    "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
    "-Ywarn-numeric-widen",     // Warn when numerics are widened.
    "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
    "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
    "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
)

scalacOptions in (Compile, doc) ++=
    Opts.doc.title(name.value)                                                                                             ++
    Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
    Seq("-sourcepath", baseDirectory.value.getAbsolutePath)

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
    filters,
    guice,
    ws,
    "org.webjars"            %  "swagger-ui"         % "3.32.3",
    "com.cnz.play"           %% "play-cnz"           % "0.1.3-SNAPSHOT",
    "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test"
)

// Silence eviction warnings
// evictionWarningOptions in update := EvictionWarningOptions.default.withWarnTransitiveEvictions(false)

// Configure Sass (https://github.com/irundaia/sbt-sassify#options).
import org.irundaia.sbt.sass.ForceScss

SassKeys.syntaxDetection := ForceScss

TwirlKeys.templateImports ++= Seq(
    "com.cnz.play.mvc.QueryFilter",
    "com.cnz.play.mvc.QueryPage",
    "com.cnz.play.mvc.QuerySort"
)

// Exclude generated files from coverage report.
coverageExcludedPackages := ";controllers\\.Reverse.*;controllers\\.javascript\\.Reverse.*;router\\.Routes.*"

fork in run := true
